#COPPER Total Cost Calculator - Coded by Keegan Griffiths

import pandas as pd
import numpy as np

writer = pd.ExcelWriter("Cost.xlsx", engine='openpyxl')

ctax={'2025':143,'2030':170,'2035':170,'2040':170,'2045':170,'2050':170}

hydro_development=True
just_small_hydro=True
flag_itc = True
flag_cer = True

configuration = pd.read_excel(r'COPPER_configuration.xlsx',header=0)

config=dict(zip(list(configuration.iloc[:]['Parameter']),list(configuration.iloc[:]['Before'])))

ap=["British Columbia", "Alberta",  "Saskatchewan", "Manitoba", "Ontario","Quebec", "New Brunswick", "Newfoundland and Labrador",  "Nova Scotia","Prince Edward Island"]
aba=["British Columbia.a", "Alberta.a",  "Saskatchewan.a", "Manitoba.a", "Ontario.a","Ontario.b","Quebec.a","Quebec.b", "New Brunswick.a", "Newfoundland and Labrador.a","Newfoundland and Labrador.b",  "Nova Scotia.a","Prince Edward Island.a"]
aba1=['a','b']
pds = ['2025','2030','2035','2040','2045','2050']
discount = 0.0374
discount_2050 = 0.374 #10x
## ITC factor for 15% discount on cap cost
factor_itc = 0.85
gridlocations = pd.read_csv(r'grid_locations.csv',header=None)
gl = list(gridlocations.iloc[:,0])
gl=[str(GL) for GL in gl]

'''
tech_evolution=pd.read_csv(r'technology_evolution.csv',header=0,index_col=0)
technology_evolution=dict()
for PD in pds:
    for GT in ['wind_ofs','wind_ons','solar']:
        technology_evolution[GT+'.'+PD]=tech_evolution[PD][GT+'.'+'base']
'''
tech_evolution=pd.read_csv(r'technology_evolution.csv',header=0,index_col=0)
technology_evolution=dict()
gen_tech_data = pd.read_csv (r'generation_type_data.csv',header=0 )
allplants_temp = list(gen_tech_data.iloc[:]['Type'])
for PD in pds:
    #for GT in ['wind_ons','wind_ofs','solar','storage_LI', 'storage_PH']:
    for GT in allplants_temp:
        technology_evolution[GT+'.'+PD]=tech_evolution[PD][GT+'.'+'base']

run_days = pd.read_csv(r'run_days-12.csv',header=None)
rundays=list(run_days.values)
rundays=[int(RD) for RD in rundays]
cap_cost_alter=(365/len(rundays))

#st=['PHS', 'LB']
#storage_hours={'PHS':8,'LB':4};
#storage_capital={'LB':215664,'PHS':141023}

st=['storage_PH', 'storage_LI']
storage_hours={'storage_PH':8,'storage_LI':4};
storage_capital={'storage_LI':158583,'storage_PH':78147}

storage_cost=dict()
for PD in pds:
    for ST in st:
        if ST+'.'+PD in technology_evolution:
            storage_cost[ST+'.'+PD]=storage_capital[ST]*technology_evolution[ST+'.'+PD]
        else:
            storage_cost[ST+'.'+PD]=storage_capital[ST]

#store_fix_o_m={'PHS.2030':9000/cap_cost_alter,'PHS.2040':9000/cap_cost_alter,'PHS.2050':9000/cap_cost_alter,'LB.2030':27580/cap_cost_alter,'LB.2040':24140/cap_cost_alter,'LB.2050':20680/cap_cost_alter,
#               'PHS.2025':9000/cap_cost_alter,'PHS.2035':9000/cap_cost_alter,'PHS.2045':9000/cap_cost_alter,'LB.2025':33885/cap_cost_alter,'LB.2035':25852/cap_cost_alter,'LB.2045':22410/cap_cost_alter}

#storage_efficiency={'PHS':0.8,'LB':0.85}

store_fix_o_m={'storage_PH.2030':14040,'storage_PH.2040':14040,'storage_PH.2050':14040,'storage_LI.2030':33748,'storage_LI.2040':33748,'storage_LI.2050':33748,
               'storage_PH.2025':14040,'storage_PH.2035':14040,'storage_PH.2045':14040,'storage_LI.2025':33748,'storage_LI.2035':33748,'storage_LI.2045':33748}

storage_efficiency={'storage_PH':0.8,'storage_LI':0.85}

runhours=rundays[-1]*24
foryear=int(config['forecast year'])
refyear=int(config['reference year'])

GJtoMWh=config['GJtoMWh']

#Generation_Type_Data = pd.read_excel(r'Generation_type_data.xlsx',header=0)
Generation_Type_Data = pd.read_csv(r'generation_type_data.csv',header=0)
Grid_Locations_Prov = pd.read_csv(r'map_gl_to_ba.csv',header=None) 
Capacity_Wind_ons = pd.read_csv(r'capacity_wind_ons.csv',header=None,index_col=0)
Capacity_Wind_ofs = pd.read_csv(r'capacity_wind_ofs.csv',header=None,index_col=0)
Capacity_Solar = pd.read_csv(r'capacity_solar.csv',header=None,index_col=0)
Capacity_Wind_ons_Recon = pd.read_csv(r'capacity_wind_ons_recon.csv',header=None,index_col=0)
Capacity_Wind_ofs_Recon = pd.read_csv(r'capacity_wind_ofs_recon.csv',header=None,index_col=0)
Capacity_Solar_Recon = pd.read_csv(r'capacity_solar_recon.csv',header=None,index_col=0)
Capacity_Thermal = pd.read_csv(r'capacity_thermal.csv',header=None,index_col=0)
Capacity_Transmission = pd.read_csv(r'capacity_transmission.csv',header=None,index_col=0)
Retire_Thermal = pd.read_csv(r'retire_thermal.csv',header=None,index_col=0)
Wind_ons_out = pd.read_csv(r'windonsout.csv',header=None,index_col=0)
Wind_ofs_out = pd.read_csv(r'windofsout.csv',header=None,index_col=0)
Solarout = pd.read_csv(r'solarout.csv',header=None,index_col=0)
DayStorageHydroout = pd.read_csv(r'daystoragehydroout.csv',header=None,index_col=0)
MonthStorageHydroout = pd.read_csv(r'monthstoragehydroout.csv',header=None,index_col=0)
Capacity_Storage = pd.read_csv(r'capacity_storage.csv',header=None,index_col=0)
ROR_Renewal_Binary = pd.read_csv(r'ror_renewal_binary.csv',header=None,index_col=0)
ROR_Renewal_Binary_no_index = pd.read_csv(r'ror_renewal_binary.csv',header=None,index_col=None)
Day_Renewal_Binary = pd.read_csv(r'day_renewal_binary.csv',header=None,index_col=0)
Day_Renewal_Binary_no_index = pd.read_csv(r'day_renewal_binary.csv',header=None,index_col=None)
Month_Renewal_Binary = pd.read_csv(r'month_renewal_binary.csv',header=None,index_col=0)
Month_Renewal_Binary_no_index = pd.read_csv(r'month_renewal_binary.csv',header=None,index_col=None)
Day_Renewal_Out = pd.read_csv(r'dayrenewalout.csv',header=None,index_col=0)
Month_Renewal_Out = pd.read_csv(r'monthrenewalout.csv',header=None,index_col=0)

# Define data types for columns by index
dtypes = {0: str, 1: float, 2: str, 3: str, 4:float }  # Replace 0 and 1 with the actual column indices

# Read the CSV file without a header and specify data types by index
Supply = pd.read_csv('supply.csv', header=None, index_col=0, dtype=dtypes)

ROR_Renewal_Binary_Dict=dict(zip(list(ROR_Renewal_Binary_no_index[0]+ROR_Renewal_Binary_no_index[1]),list(ROR_Renewal_Binary.iloc[:,-1])))
Month_Renewal_Binary_Dict=dict(zip(list(Month_Renewal_Binary_no_index[0]+Month_Renewal_Binary_no_index[1]),list(Month_Renewal_Binary_no_index.iloc[:,-1])))
Day_Renewal_Binary_Dict=dict(zip(list(Day_Renewal_Binary_no_index[0]+Day_Renewal_Binary_no_index[1]),list(Day_Renewal_Binary_no_index.iloc[:,-1])))

transmapba=pd.read_csv(r'transmission_map_ba.csv',header=None)
transmap=list(transmapba.iloc[:,0])
del transmapba

transmapdis = pd.read_csv(r'transmission_map_distance.csv',header=None)
distance=dict(transmapdis.values)
del transmapdis



GL_to_ABA = {str(row[1][0]):row[1][1] for row in Grid_Locations_Prov.iterrows()}

h=list(range(1,8761))

allplants=list(Generation_Type_Data.iloc[:]['Type'])

tplants=list()
isthermal=list(Generation_Type_Data.iloc[:]['Is thermal?'])
cc=0
for i in isthermal:
    if i:
        tplants.append(allplants[cc])
    cc+=1

ITCplants=list()
if flag_itc:
    isITCsupport=list(Generation_Type_Data.iloc[:]['ITC support?'])
    cc=0
    for i in isITCsupport:
        if i:
            ITCplants.append(allplants[cc])
        cc+=1
    print('at start of ITCplants!')
    print(ITCplants)

efficiency=dict(zip(list(Generation_Type_Data.iloc[:]['Type']),list(Generation_Type_Data.iloc[:]['efficiency'])))

fixed_o_m=dict(zip(list(Generation_Type_Data.iloc[:]['Type']),list(Generation_Type_Data.iloc[:]['fixed_o_m'])))
#fixed_o_m_raw = fixed_o_m.copy()

#Omar: That should not be a thing!!!
#for k in fixed_o_m:
#    fixed_o_m[k]=fixed_o_m[k]/cap_cost_alter
  
variable_o_m=dict(zip(list(Generation_Type_Data.iloc[:]['Type']),list(Generation_Type_Data.iloc[:]['variable_o_m'])))
fuelprice=dict(zip(list(Generation_Type_Data.iloc[:]['Type']),list(Generation_Type_Data.iloc[:]['fuelprice']))) 

trans_o_m=config['trans_o_m']
transcost=config['transcost']
intra_ba_transcost=config['intra_ba_transcost']


Capacity_TP_alt = pd.read_excel(r'Results_summary.xlsx', header=0, sheet_name='ABA_generation_mix')

new_installed_Capacity_TP = pd.read_excel(r'Results_summary.xlsx', header=0, sheet_name='New_installed_ABA')
new_installed_Capacity_TP.drop('Unnamed: 0',axis='columns', inplace=True)

Capacity_TP_dict = dict()
for data in Capacity_TP_alt.values:
    Capacity_TP_dict.update({str(data[0])+'.'+data[1]+'.'+data[2]:data[3]})

new_installed_Capacity_TP_dict = dict()
for data in new_installed_Capacity_TP.values:
    new_installed_Capacity_TP_dict.update({str(data[0])+'.'+str(data[1])+'.'+data[2]:data[3]})

distancetogrid = pd.read_csv(r'distance_to_grid.csv',header=None)
distance_to_grid = dict(distancetogrid.values)
windonscost = {}
windofscost = {}
solarcost = {}
capacitycost = {}
transmissioncost = {}
for PD in pds:
    for GL in gl:
        #
        windonscost[PD+'.'+GL] = Generation_Type_Data.at[Generation_Type_Data[Generation_Type_Data['Type'] == 'solar'].index[0],"capitalcost"]*technology_evolution['wind_ons.'+PD] + distance_to_grid[int(GL)]*intra_ba_transcost
        windofscost[PD+'.'+GL] = Generation_Type_Data.at[Generation_Type_Data[Generation_Type_Data['Type'] == 'solar'].index[0],"capitalcost"]*technology_evolution['wind_ofs.'+PD] + distance_to_grid[int(GL)]*intra_ba_transcost
        solarcost[PD+'.'+GL] = Generation_Type_Data.at[Generation_Type_Data[Generation_Type_Data['Type'] == 'wind_ons'].index[0],"capitalcost"]*technology_evolution['solar.'+PD] + distance_to_grid[int(GL)]*intra_ba_transcost
    for ABA in aba:
        for TP in tplants:
            capacitycost[PD,ABA,TP] = Generation_Type_Data.at[Generation_Type_Data[Generation_Type_Data['Type'] == TP].index[0],"capitalcost"] * technology_evolution[TP+'.'+PD]
        for ABBA in aba:
            if ABA+'.'+ABBA in transmap:
                transmissioncost[PD,ABA,ABBA] = transcost*distance[ABA+'.'+ABBA]
    
carbondioxide=dict(zip(list(Generation_Type_Data.iloc[:]['Type']),list(Generation_Type_Data.iloc[:]['fuel_co2'])))
gasprice={"British Columbia.a":2.69, "Alberta.a":2.60,  "Saskatchewan.a":2.55, "Manitoba.a":2.73, "Ontario.a":6.77,"Ontario.b":6.77,"Quebec.a":6.73,"Quebec.b":6.73, "New Brunswick.a":6.21, "Newfoundland and Labrador.a":7.39,"Newfoundland and Labrador.b":7.39,  "Nova Scotia.a":7.39,"Prince Edward Island.a":7.39}
    
gas_tech_all = ['gasSC_pre2025', \
    'gasSC_ccs_pre2025', \
    'gasSC_backup_pre2025', \
    'gasSC_backup_post2025', \
    'gasCCS_post2025', \
    'gasCC_pre2025', \
    'gasCC_ccs_pre2025', \
    'gasCC_backup_pre2025', \
    'gasCC_backup_post2025', \
    'gasCC_free_post2025', \
    'gasCG_free_pre2025', \
    'gasCG_restricted_pre2025', \
    'gasCG_restricted_ccs_pre2025', \
    'gasCG_restricted_backup_pre2025', \
    ]

fuelcost=dict()
for PD in pds:
    for TP in tplants:
        for ABA in aba:
            ##JGM Fix Below
            #if TP=='gasCC_pre2025' or TP=='gasCC_ccs_pre2025' or TP=='gasCC_backup_pre2025' or TP=='gasCC_backup_post2025' or TP=='gasSC_pre2025' or TP=='gasSC_ccs_pre2025' or TP=='gasSC_backup_pre2025' or TP=='gasSC_backup_post2025':
            if TP in gas_tech_all:
                fuelcost[PD+'.'+TP+'.'+ABA] = (gasprice[ABA]/efficiency[TP])*GJtoMWh
            else:
                fuelcost[PD+'.'+TP+'.'+ABA] = (fuelprice[TP]/efficiency[TP])*GJtoMWh

extantwindsolar = pd.read_csv(r'extant_wind_solar.csv',header=0)
extant_wind_solar=list()
for PD in pds:
    extant_wind_solar.append(dict(zip(list(extantwindsolar.iloc[:]['location']),list(extantwindsolar.iloc[:][PD]))))

del extantwindsolar
            
extantcapacity = pd.read_csv(r'extant_capacity_CER.csv',header=0)
extant_capacity=dict()
for PD in pds:
    label_ABA=list(extantcapacity.iloc[:]['ABA'])
    label_ABA=[PD+'.'+LL for LL in label_ABA]
    excap=dict(zip(label_ABA,list(extantcapacity.iloc[:][PD])))
    extant_capacity.update(excap)
    
extanttrans = pd.read_csv(r'extant_transmission.csv',header=0)
extant_transmission=list()
for PD in pds:
    extant_transmission.append(dict(zip(list(extanttrans.iloc[:]['ABA']),list(extanttrans.iloc[:][PD]))))
del extanttrans
    
hydrocf = pd.read_csv(r'hydro_cf.csv',header=None)
hydro_cf=dict(hydrocf.values)
del hydrocf

maphd = pd.read_csv(r'set_map_days_to_hours.csv',header=None)
map_hd=dict(maphd.values)
del maphd

maphm = pd.read_csv(r'set_map_months_to_hours.csv',header=None)
map_hm=dict(maphm.values)
del maphm

nummonths=map_hm[rundays[-1]*24]
m=list(range(1,nummonths+1))

d=rundays.copy()
h3=h.copy()
for H in h3:
    if map_hd[H] not in rundays:
        h.remove(H)

del h3
    
extant_thermal=dict()
for AP in ap:
    for ABA in aba1:
        for TP in tplants:
            for PD in pds:
                extant_thermal[PD+'.'+AP+'.'+ABA+'.'+TP]=0
                if PD+'.'+AP+'.'+ABA+'.'+TP in extant_capacity:
                    extant_thermal[PD+'.'+AP+'.'+ABA+'.'+TP]=extant_capacity[PD+'.'+AP+'.'+ABA+'.'+TP]
                if TP=='coal' and int(PD)>=2030:
                    extant_thermal[PD+'.'+AP+'.'+ABA+'.'+TP]=0

hydro_capacity=dict()
extant_wind_gen=dict()
extant_solar_gen=dict()
for PD in pds:
    for AP in ap:
        for ABA in aba1:
    
            for H in h:
                extant_wind_gen[PD+'.'+AP+'.'+ABA+'.'+str(H)]=0
                extant_solar_gen[PD+'.'+AP+'.'+ABA+'.'+str(H)]=0
    
ror_hydroout=dict()
day_hydroout=dict()
month_hydroout=dict()
ror_hydro_capacity=dict()
day_hydro_capacity=dict()
month_hydro_capacity=dict()
for PD in pds:
    for AP in ap:
        for ABA in aba1:
            ror_hydro_capacity[PD+'.'+AP+'.'+ABA] = 0
            day_hydro_capacity[PD+'.'+AP+'.'+ABA] = 0
            month_hydro_capacity[PD+'.'+AP+'.'+ABA] = 0
            if PD+'.'+AP+'.'+ABA+'.'+'hydro_run' in extant_capacity:
                ror_hydro_capacity[PD+'.'+AP+'.'+ABA] = extant_capacity[PD+'.'+AP+'.'+ABA+'.'+'hydro_run']
                day_hydro_capacity[PD+'.'+AP+'.'+ABA] = extant_capacity[PD+'.'+AP+'.'+ABA+'.'+'hydro_daily']
                month_hydro_capacity[PD+'.'+AP+'.'+ABA] = extant_capacity[PD+'.'+AP+'.'+ABA+'.'+'hydro_monthly']
            for H in h:
                if AP+'.'+str(H) in hydro_cf:
                    ror_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA]=ror_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_cf[AP+'.'+str(H)]
                    day_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] = day_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_cf[AP+'.'+str(H)]
                    month_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] = month_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_cf[AP+'.'+str(H)]
                else:
                    ror_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] =0
                    day_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] =0
                    month_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] =0
short_name_to_ABA = {}                  
## hydro renewal and greenfield development necessary inputs 
if hydro_development:
    if not just_small_hydro:
        #hydro_new = pd.read_excel (r'hydro_new_recon.xlsx',header=0)
        hydro_new = pd.read_csv (r'hydro_new.csv',header=0)
    elif just_small_hydro:
        hydro_new = pd.read_csv (r'hydro_new.csv',header=0)
        
    
    for data, row in hydro_new.T.iteritems():
        short_name_to_ABA.update({row['Short Name']:row['Balancing Area']})
    
    hydro_renewal=list(hydro_new.iloc[:]['Short Name'])
    cost_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Annualized Capital Cost ($M/year)'])))
    capacity_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Additional Capacity (MW)'])))
    devperiod_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Development Time (years)'])))
    location_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Balancing Area'])))
    distance_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Distance to Grid (km)'])))
    type_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Generation Type - COPPER'])))
    fixed_o_m_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Fixed O&M ($/MW-year)'])))
    variable_o_m_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Variable O&M ($/MWh)'])))
    
    hr_ror=list()
    cost_ror_renewal=dict()
    capacity_ror_renewal=dict()
    hr_ror_location=dict()
    
    hr_day=list()
    cost_day_renewal=dict()
    capacity_day_renewal=dict()
    hr_day_location=dict()
    
    hr_mo=list()
    cost_month_renewal=dict()
    capacity_month_renewal=dict()
    hr_month_location=dict()
    
    hr_pump=list()
    cost_pump_renewal=dict()
    capacity_pump_renewal=dict()
    hr_pump_location=dict()
    for k in hydro_renewal:
        if foryear-2020>=devperiod_renewal[k]:
            if type_renewal[k]=='hydro_run':
                hr_ror.append(k)
                cost_ror_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_ror_renewal[k]=capacity_renewal[k]
                hr_ror_location[k]=location_renewal[k]
    
            if type_renewal[k]=='hydro_daily':
                hr_day.append(k)
                cost_day_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_day_renewal[k]=capacity_renewal[k]
                hr_day_location[k]=location_renewal[k]
    
            if type_renewal[k]=='hydro_monthly':
                hr_mo.append(k)
                cost_month_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_month_renewal[k]=capacity_renewal[k]
                hr_month_location[k]=location_renewal[k]
            if type_renewal[k]=='hydro_pump':
                hr_pump.append(k)
                cost_pump_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_pump_renewal[k]=capacity_renewal[k]
                hr_pump_location[k]=location_renewal[k]
           
    ror_renewalout=dict()
    for HR_ROR in hr_ror:
        for H in h:
            province_loc=hr_ror_location[HR_ROR].replace('.a','')
            province_loc=province_loc.replace('.b','')
            ror_renewalout[str(H)+'.'+HR_ROR]=capacity_ror_renewal[HR_ROR]*hydro_cf[province_loc+'.'+str(H)]
    
    for k in cost_ror_renewal:
        cost_ror_renewal[k]=cost_ror_renewal[k]    
            
    
    day_renewal_historic=dict()
    day_renewalout=dict()
    for HR_DAY in hr_day:
        for D in d:
            day_renewal_historic[str(D)+'.'+HR_DAY]=0 
        for H in h:
            province_loc=hr_day_location[HR_DAY].replace('.a','')
            province_loc=province_loc.replace('.b','')
            day_renewalout[str(H)+'.'+HR_DAY]=capacity_day_renewal[HR_DAY]*hydro_cf[province_loc+'.'+str(H)]
            day_renewal_historic[str(map_hd[H])+'.'+HR_DAY]=day_renewal_historic[str(map_hd[H])+'.'+HR_DAY]+day_renewalout[str(H)+'.'+HR_DAY]
    
    for k in cost_day_renewal:
        cost_day_renewal[k]=cost_day_renewal[k]
    
    
    month_renewal_historic=dict()
    month_renewalout=dict()
    for HR_MO in hr_mo:
        for M in m:
            month_renewal_historic[str(M)+'.'+HR_MO]=0
        for H in h:
            province_loc=hr_month_location[HR_MO].replace('.a','')
            province_loc=province_loc.replace('.b','')
            month_renewalout[str(H)+'.'+HR_MO]=capacity_month_renewal[HR_MO]*hydro_cf[province_loc+'.'+str(H)]
            month_renewal_historic[str(map_hm[H])+'.'+HR_MO]=month_renewal_historic[str(map_hm[H])+'.'+HR_MO]+month_renewalout[str(H)+'.'+HR_MO]
    
    for k in cost_month_renewal:
        cost_month_renewal[k]=cost_month_renewal[k]
    
    
    for k in cost_pump_renewal:
        cost_pump_renewal[k]=cost_pump_renewal[k]

windonscost_recon=dict()
windofscost_recon=dict()
solarcost_recon=dict()

for PD in pds:
    windonscost_recon[PD]=config['windcost_recon']*technology_evolution['wind_ons.'+PD]
    windofscost_recon[PD]=config['windcost_recon']*technology_evolution['wind_ofs.'+PD]
    solarcost_recon[PD]=config['solarcost_recon']*technology_evolution['solar.'+PD]


#check for itc and pds if they are 2025 or 2030: 
def cost_capacity(TP_cost, Capacity_TP_in_scope, pd_only=False):
    
    capacities = []
    for PD_index in range(len(pds)):
        capacities.append({ABA: 0 for ABA in aba})
        for GL in gl:
            ABA = GL_to_ABA[GL]
            if flag_itc and pds[PD_index] in ['2025', '2030']:
                if pd_only is False:
                    capacities[PD_index][ABA] += TP_cost[pds[PD_index]+'.'+GL]*capacity_per_grid_cell(Capacity_TP_in_scope, pds[PD_index], GL)*factor_itc
                else:
                    capacities[PD_index][ABA] += TP_cost[pds[PD_index]]*capacity_per_grid_cell(Capacity_TP_in_scope, pds[PD_index], GL)*factor_itc
            else:
                if pd_only is False:
                    capacities[PD_index][ABA] += TP_cost[pds[PD_index]+'.'+GL]*capacity_per_grid_cell(Capacity_TP_in_scope, pds[PD_index], GL)
                else:
                    capacities[PD_index][ABA] += TP_cost[pds[PD_index]]*capacity_per_grid_cell(Capacity_TP_in_scope, pds[PD_index], GL)

    return capacities

def FixedOM_cost_capacity(TP_cost, Capacity_TP_in_scope):
    capacities = []
    for PD_index in range(len(pds)):
        capacities.append({ABA: 0 for ABA in aba})
        for GL in gl:
            ABA = GL_to_ABA[GL]
            capacities[PD_index][ABA] += FixedOM_capacity_per_grid_cell(TP_cost, Capacity_TP_in_scope, pds[PD_index], GL, 'wind', 'wind_ons')
            capacities[PD_index][ABA] += FixedOM_capacity_per_grid_cell(TP_cost, Capacity_TP_in_scope, pds[PD_index], GL, 'wind', 'wind_ofs')

    return capacities


# Calculate the costs for given period
def cost_calculation(PD, pd_index):
    
    Sum_Dict = {
    "Cap Cost": "", "Carbon Cost": {ABA: 0 for ABA in aba},"Wind_ons": 0,"Wind_ofs": 0, "Solar": 0, "Wind_ons_Recon": 0, "Wind_ofs_Recon": 0, "Solar_Recon": 0, "Transmission": {ABA: 0 for ABA in aba}, "---------------": "------------", 
    "F Cost": "", "Supply": {ABA: 0 for ABA in aba}, "----------------": "------------","FixedOM Storage":{ABA: 0 for ABA in aba},
    "Fixed OM": {ABA: 0 for ABA in aba}, "Fixed_OM_Thermal": {ABA: 0 for ABA in aba}, "FixedOM Wind_ons": 0, "FixedOM Wind_ofs": 0, "FixedOM Solar": 0, "FixedOM Transmission": {ABA: 0 for ABA in aba}, "Extant Transmission": {ABA: 0 for ABA in aba}, "ROR Hydro Capacity": {ABA: 0 for ABA in aba}, "Day Hydro Capacity": {ABA: 0 for ABA in aba}, "Month Hydro Capacity": {ABA: 0 for ABA in aba}, "-----------------": "------------",
    "Variable OM Supply": {ABA: 0 for ABA in aba}, "Wind_ons Out": {ABA: 0 for ABA in aba}, "Wind_ofs Out": {ABA: 0 for ABA in aba}, "Solar Out": {ABA: 0 for ABA in aba}, "ROR Hydro Out": {ABA: 0 for ABA in aba}, "Day Hydro Out": {ABA: 0 for ABA in aba}, "Month Hydro Out": {ABA: 0 for ABA in aba}, "------------------": "------------","Hydro Capital Cost": {ABA: 0 for ABA in aba}, "Hydro Renewal Cost": {ABA: 0 for ABA in aba}, "-------------------": "------------",
    "Hydro Renewal Cost": "", "ROR Hydro Renewal": {ABA: 0 for ABA in aba}, "Day Hydro Renewal": {ABA: 0 for ABA in aba}, "Month Hydro Renewal": {ABA: 0 for ABA in aba}, "-------------------": "------------",
    "New Storage Cost": "", "New Storage Capacity": {ABA: 0 for ABA in aba}, "--------------------": "------------", "coal_pre2025": {ABA: 0 for ABA in aba}, "coal_fuelblending_pre2025": {ABA: 0 for ABA in aba}, "coal_ccs_pre2025": {ABA: 0 for ABA in aba},"coal_backup_pre2025": {ABA: 0 for ABA in aba}, "coal_retire_pre2025": {ABA: 0 for ABA in aba}, "diesel_pre2025": {ABA: 0 for ABA in aba}, "diesel_fuelblending_pre2025": {ABA: 0 for ABA in aba}, 
    "diesel_ccs_pre2025": {ABA: 0 for ABA in aba}, "diesel_backup_pre2025": {ABA: 0 for ABA in aba}, "diesel_retire_pre2025": {ABA: 0 for ABA in aba}, "gasSC_pre2025": {ABA: 0 for ABA in aba}, "gasSC_fuelblending_pre2025": {ABA: 0 for ABA in aba}, "gasSC_ccs_pre2025": {ABA: 0 for ABA in aba}, "gasSC_backup_pre2025": {ABA: 0 for ABA in aba}, "gasSC_retire_pre2025": {ABA: 0 for ABA in aba}, "gasCCS_post2025": {ABA: 0 for ABA in aba}, "gasCC_pre2025": {ABA: 0 for ABA in aba},
    "gasCC_fuelblending_pre2025": {ABA: 0 for ABA in aba}, "gasCC_ccs_pre2025": {ABA: 0 for ABA in aba}, "gasCC_backup_pre2025": {ABA: 0 for ABA in aba}, "gasCC_retire_pre2025": {ABA: 0 for ABA in aba}, "nuclear": {ABA: 0 for ABA in aba}, "nuclear_SMR": {ABA: 0 for ABA in aba}, "h2blue_CT": {ABA: 0 for ABA in aba}, "h2green_CT": {ABA: 0 for ABA in aba}, "biomass": {ABA: 0 for ABA in aba}, "Present":{ABA: 0 for ABA in aba}, "Total":{ABA: 0 for ABA in aba}}
    
    for TP in tplants:
        Sum_Dict.update({TP: {ABA: 0 for ABA in aba}})

    for TP in tplants:
        Sum_Dict.update({"Fixed_OM"+'.'+TP: {ABA: 0 for ABA in aba}})

    for TP in tplants:
        Sum_Dict.update({"Variable_OM"+'.'+TP: {ABA: 0 for ABA in aba}})

    for ABA in aba:

        for TP in tplants:
            Capacity_Thermal_Temp = Capacity_Thermal.loc[Capacity_Thermal.index == int(pds[pd_index])]
            Capacity_Thermal_Temp = Capacity_Thermal_Temp.loc[Capacity_Thermal_Temp.iloc[:,0] == ABA]
            Capacity_Thermal_Temp = Capacity_Thermal_Temp.loc[Capacity_Thermal_Temp.iloc[:,1] == TP]
            #C_T_T = Capacity_Thermal.loc[Capacity_Thermal.index == int(PD)].loc[Capacity_Thermal_Temp.iloc[:,0] == ABA].loc[Capacity_Thermal_Temp.iloc[:,1] == TP]

            key = Capacity_Thermal_Temp.iloc[0,1]
            if PD in ['2025' or '2030'] and flag_itc and TP in ITCplants:
                #Sum_Dict[key][ABA] += capacitycost[PD,ABA,TP]*Capacity_Thermal_Temp.iloc[0,2]*factor_itc
                Sum_Dict[key][ABA] += capacitycost[PD,ABA,TP]*new_installed_Capacity_TP_dict[PD+'.'+TP.lower()+'.'+ABA.lower()]*factor_itc
            else:
                #Sum_Dict[key][ABA] += capacitycost[PD,ABA,TP]*Capacity_Thermal_Temp.iloc[0,2]
                Sum_Dict[key][ABA] += capacitycost[PD,ABA,TP]*new_installed_Capacity_TP_dict[PD+'.'+TP.lower()+'.'+ABA.lower()]
            
            Supply_Temp = Supply.loc[Supply.index == int(pds[pd_index])]
            Supply_Temp = Supply_Temp.loc[Supply_Temp.iloc[:,1] == ABA]
            Supply_Temp = Supply_Temp.loc[Supply_Temp.iloc[:,2] == TP]

            ## Is this performed twice? It looks like we calculate VOM for thermal plants further below
            for index,row in Supply_Temp.iterrows():
                Sum_Dict["Supply"][ABA] += fuelcost[pds[pd_index]+'.'+TP+'.'+ABA]*row[4]*cap_cost_alter
                Sum_Dict["Variable OM Supply"][ABA] += variable_o_m[TP]*row[4]*cap_cost_alter
                
            Retire_Thermal_Temp = Retire_Thermal.loc[Retire_Thermal.index == int(pds[pd_index])]
            Retire_Thermal_Temp = Retire_Thermal_Temp.loc[Retire_Thermal_Temp.iloc[:,0] == ABA]
            Retire_Thermal_Temp = Retire_Thermal_Temp.loc[Retire_Thermal_Temp.iloc[:,1] == TP]
            #Retire_Thermal_Temp = Retire_Thermal.loc[Retire_Thermal.index == int(PD)].loc[Retire_Thermal_Temp.iloc[:,0] == ABA].loc[Retire_Thermal_Temp.iloc[:,1] == TP]
            Sum_Dict["Fixed_OM_Thermal"][ABA] += (extant_thermal[pds[0]+'.'+ABA+'.'+TP] + sum([new_installed_capacity_thermal_TP_ABA_PD2(PD2, ABA, TP)  - retire_therm_TP_ABA_PD2(PD2, ABA, TP) for PD2 in pds [:pds.index(PD)+1]]))*fixed_o_m[TP]
            Sum_Dict["Fixed_OM"+'.'+TP][ABA] = (extant_thermal[pds[0]+'.'+ABA+'.'+TP] + sum([new_installed_capacity_thermal_TP_ABA_PD2(PD2, ABA, TP)  - retire_therm_TP_ABA_PD2(PD2, ABA, TP) for PD2 in pds [:pds.index(PD)+1]]))*fixed_o_m[TP]
            #[Capacity_Thermal.loc[Capacity_Thermal.index == int(PD2)].loc[Capacity_Thermal_Temp.iloc[:,0] == ABA].loc[Capacity_Thermal_Temp.iloc[:,1] == TP].iloc[0,2]-Retire_Thermal.loc[Retire_Thermal.index == int(PD2)].loc[Retire_Thermal_Temp.iloc[:,0] == ABA].loc[Retire_Thermal_Temp.iloc[:,1] == TP].iloc[0,2] for PD2 in pds[:pds.indexOf(PD)] ]
        
        Windonsout_Temp = Wind_ons_out.loc[Wind_ons_out.index == int(PD)]
        Windonsout_Temp = Windonsout_Temp.loc[Windonsout_Temp.iloc[:,1] == ABA]
        for index,row in Windonsout_Temp.iterrows():
            Sum_Dict["Wind_ons Out"][ABA] += row[3]*variable_o_m['wind_ons']*cap_cost_alter

        for index,row in Windonsout_Temp.iterrows():
            Sum_Dict["Wind_ofs Out"][ABA] += row[3]*variable_o_m['wind_ofs']*cap_cost_alter
        
        Solarout_Temp = Solarout.loc[Solarout.index == int(PD)]
        Solarout_Temp = Solarout_Temp.loc[Solarout_Temp.iloc[:,1] == ABA]
        for index,row in Solarout_Temp.iterrows():
            Sum_Dict["Solar Out"][ABA] += row[3]*variable_o_m['solar']*cap_cost_alter
        
        for H in h:
            Sum_Dict["ROR Hydro Out"][ABA] += ror_hydroout[PD+'.'+str(H)+'.'+ABA]*variable_o_m['hydro_run']*cap_cost_alter
            
        DayStorageHydroout_Temp = DayStorageHydroout.loc[DayStorageHydroout.index == int(PD)]
        DayStorageHydroout_Temp = DayStorageHydroout_Temp.loc[DayStorageHydroout_Temp.iloc[:,1] == ABA]
        for _, row in DayStorageHydroout_Temp.iterrows():
            Sum_Dict["Day Hydro Out"][ABA] += row[3]*variable_o_m['hydro_run']*cap_cost_alter
        
        MonthStorageHydroout_Temp = MonthStorageHydroout.loc[MonthStorageHydroout.index == int(PD)]
        MonthStorageHydroout_Temp = MonthStorageHydroout_Temp.loc[MonthStorageHydroout_Temp.iloc[:,1] == ABA]
        for _, row in MonthStorageHydroout_Temp.iterrows():
            Sum_Dict["Month Hydro Out"][ABA] += row[3]*variable_o_m['hydro_run']*cap_cost_alter
            
        for ABBA in aba:
            if ABA+'.'+ABBA in transmap:
                Capacity_Transmission_Temp = Capacity_Transmission.loc[Capacity_Transmission.index == int(PD)]
                Capacity_Transmission_Temp = Capacity_Transmission_Temp.loc[Capacity_Transmission_Temp.iloc[:,0] == ABA]
                Capacity_Transmission_Temp = Capacity_Transmission_Temp.loc[Capacity_Transmission_Temp.iloc[:,1] == ABBA]
                Sum_Dict["Transmission"][ABA] += transmissioncost[PD,ABA,ABBA]*Capacity_Transmission_Temp.iloc[0,2]
                
                Sum_Dict["FixedOM Transmission"][ABA] += Capacity_Transmission_Temp.iloc[0,2]*trans_o_m
                
            if ABA+'.'+ABBA in extant_transmission[pds.index(PD)]:
                Sum_Dict["Extant Transmission"][ABA] += extant_transmission[pds.index(PD)][ABA+'.'+ABBA]*trans_o_m
            
        Sum_Dict["ROR Hydro Capacity"][ABA] += ror_hydro_capacity[PD+'.'+ABA] * fixed_o_m['hydro_run']
        Sum_Dict["Day Hydro Capacity"][ABA] += day_hydro_capacity[PD+'.'+ABA] * fixed_o_m['hydro_run']
        Sum_Dict["Month Hydro Capacity"][ABA] += month_hydro_capacity[PD+'.'+ABA] * fixed_o_m['hydro_run']
    
        for ST in st:
            Capacity_Storage_Temp = Capacity_Storage.loc[Capacity_Storage.index == int(PD)]
            Capacity_Storage_Temp = Capacity_Storage_Temp.loc[Capacity_Storage_Temp.iloc[:,0] == ST]
            Capacity_Storage_Temp = Capacity_Storage_Temp.loc[Capacity_Storage_Temp.iloc[:,1] == ABA]
            Sum_Dict["FixedOM Storage"][ABA]+= Capacity_Storage_Temp.iloc[0,2]*store_fix_o_m[ST+'.'+PD]
            if PD in ['2025' or '2030'] and flag_itc:
                ##look here!
                Sum_Dict["New Storage Capacity"][ABA] += Capacity_Storage_Temp.iloc[0,2]*((storage_cost[ST+'.'+PD] * factor_itc) + store_fix_o_m[ST+'.'+PD])
            else:
                Sum_Dict["New Storage Capacity"][ABA] += Capacity_Storage_Temp.iloc[0,2]*(storage_cost[ST+'.'+PD] + store_fix_o_m[ST+'.'+PD])
    
    if hydro_development:
        
        
        for HR_ROR in hr_ror:
            ROR_Renewal_Binary_Temp = ROR_Renewal_Binary.loc[ROR_Renewal_Binary.index == "('"+PD+"'"]
            ROR_Renewal_Binary_Temp = ROR_Renewal_Binary_Temp.loc[ROR_Renewal_Binary_Temp.iloc[:,0] == " '"+HR_ROR+"')"]
            #check if flag itc and if ps is in [2025]
            #Sum_Dict["ROR Hydro Renewal"][short_name_to_ABA[HR_ROR]] += cost_ror_renewal[HR_ROR]*ROR_Renewal_Binary_Temp.iloc[0,1]*factor_itc
            if PD in ['2025' or '2030'] and flag_itc:
                Sum_Dict["ROR Hydro Renewal"][short_name_to_ABA[HR_ROR]] += cost_ror_renewal[HR_ROR]*ROR_Renewal_Binary_Temp.iloc[0,1]*factor_itc
            else:
                Sum_Dict["ROR Hydro Renewal"][short_name_to_ABA[HR_ROR]] += cost_ror_renewal[HR_ROR]*ROR_Renewal_Binary_Temp.iloc[0,1]

            Sum_Dict["ROR Hydro Renewal"][short_name_to_ABA[HR_ROR]] += capacity_ror_renewal[HR_ROR]*ROR_Renewal_Binary_Temp.iloc[0,1]*fixed_o_m_renewal[HR_ROR] 
            Sum_Dict["ROR Hydro Renewal"][short_name_to_ABA[HR_ROR]] += ror_renewalout[str(H)+'.'+HR_ROR]*ROR_Renewal_Binary_Temp.iloc[0,1]*variable_o_m_renewal[HR_ROR]*cap_cost_alter
        
        for HR_DAY in hr_day:
            Day_Renewal_Binary_Temp = Day_Renewal_Binary.loc[Day_Renewal_Binary.index == "('"+PD+"'"]
            Day_Renewal_Binary_Temp = Day_Renewal_Binary_Temp.loc[Day_Renewal_Binary_Temp.iloc[:,0] == " '"+HR_DAY+"')"]
            if PD in ['2025' or '2030'] and flag_itc:
                Sum_Dict["Day Hydro Renewal"][short_name_to_ABA[HR_DAY]] += cost_day_renewal[HR_DAY]*Day_Renewal_Binary_Temp.iloc[0,1]*factor_itc
            else:
                Sum_Dict["Day Hydro Renewal"][short_name_to_ABA[HR_DAY]] += cost_day_renewal[HR_DAY]*Day_Renewal_Binary_Temp.iloc[0,1]
            Sum_Dict["Day Hydro Renewal"][short_name_to_ABA[HR_DAY]] += capacity_day_renewal[HR_DAY]*Day_Renewal_Binary_Temp.iloc[0,1]*fixed_o_m_renewal[HR_DAY]
            Day_Renewal_Out_Temp = Day_Renewal_Out.loc[Day_Renewal_Out.index == int(PD)]
            Day_Renewal_Out_Temp = Day_Renewal_Out_Temp.loc[Day_Renewal_Out_Temp.iloc[:,1] == HR_DAY]
            for index,row in Day_Renewal_Out_Temp.iterrows():
                Sum_Dict["Day Hydro Renewal"][short_name_to_ABA[HR_DAY]] += row[3]*variable_o_m_renewal[HR_DAY]*cap_cost_alter
            
        for HR_MO in hr_mo:
            Month_Renewal_Binary_Temp = Month_Renewal_Binary.loc[Month_Renewal_Binary.index == "('"+PD+"'"]
            Month_Renewal_Binary_Temp = Month_Renewal_Binary_Temp.loc[Month_Renewal_Binary_Temp.iloc[:,0] == " '"+HR_MO+"')"]
            if PD in ['2025' or '2030'] and flag_itc:
                Sum_Dict["Month Hydro Renewal"][short_name_to_ABA[HR_MO]] += cost_month_renewal[HR_MO]*Month_Renewal_Binary_Temp.iloc[0,1]*factor_itc
            else:
                Sum_Dict["Month Hydro Renewal"][short_name_to_ABA[HR_MO]] += cost_month_renewal[HR_MO]*Month_Renewal_Binary_Temp.iloc[0,1]            
            Sum_Dict["Month Hydro Renewal"][short_name_to_ABA[HR_MO]] += capacity_month_renewal[HR_MO]*Month_Renewal_Binary_Temp.iloc[0,1]*fixed_o_m_renewal[HR_MO]
            Month_Renewal_Out_Temp = Month_Renewal_Out.loc[Month_Renewal_Out.index == int(PD)]
            Month_Renewal_Out_Temp = Month_Renewal_Out_Temp.loc[Month_Renewal_Out_Temp.iloc[:,1] == HR_MO]
            for index,row in Month_Renewal_Out_Temp.iterrows():
                Sum_Dict["Month Hydro Renewal"][short_name_to_ABA[HR_MO]] += row[3]*variable_o_m_renewal[HR_MO]*cap_cost_alter
            
    return Sum_Dict

def retire_therm_TP_ABA_PD2(PD2, ABA, TP):
    Retire_Thermal_Temp = Retire_Thermal.loc[Retire_Thermal.index == int(PD2)]
    Retire_Thermal_Temp = Retire_Thermal_Temp.loc[Retire_Thermal_Temp.iloc[:,0] == ABA]
    Retire_Thermal_Temp = Retire_Thermal_Temp.loc[Retire_Thermal_Temp.iloc[:,1] == TP]
    try:
        return Retire_Thermal_Temp.iloc[0,2]
    except IndexError:
        return 0

def new_installed_capacity_thermal_TP_ABA_PD2(PD2, ABA, TP):
    return new_installed_Capacity_TP_dict[PD2+'.'+TP.lower()+'.'+ABA.lower()]
    

def FixedOM_capacity_per_grid_cell(TP, TP_Recon, PD, GL, key, key2):
    capacity_per_grid_cell = TP.iloc[TP.index == "('" + PD + "'"].iat[int(GL)-1,1]+TP_Recon.iloc[TP_Recon.index == "('" + PD + "'"].iat[int(GL)-1,1]*fixed_o_m[key2]
    if str(GL)+'.'+key in extant_wind_solar[0]:
        capacity_per_grid_cell += extant_wind_solar[pds.index(PD)][str(GL)+'.'+key]*fixed_o_m[key2]
    return capacity_per_grid_cell

def capacity_per_grid_cell(Capacity_per_gl, PD, GL):
    return Capacity_per_gl.iloc[Capacity_per_gl.index == "('" + PD + "'"].iat[int(GL)-1,1]


# Calculate the present value from an annuity
def annuity_to_present(C, i, n):

    Sum = 1 + i
    
    for x in range(1,n):
        Sum *= 1 + i
    
    Sum = 1 / Sum
    Sum = C * (1 - Sum) / i
    
    return Sum


# Calculate the present value from a future value
def future_to_present(C, i, n):

    Sum = 1 + i
    
    for x in range(1,n):
        Sum *= 1 + i
    
    Sum = C / Sum
    
    return Sum

import re
def main():
    
    Hydro_Capital_Cost_per_aba = {PD+'.'+ABA:0 for ABA in aba for PD in pds}

    for PD in pds:
        for project_name in list(ROR_Renewal_Binary_Dict.keys()):
            matches = re.findall(r"[A-Z_\d]+\'\)", project_name)
            Hydro_Capital_Cost_per_aba[PD+'.'+short_name_to_ABA[matches[0][:-2]]] +=( ROR_Renewal_Binary_Dict[project_name])* cost_renewal [matches[0][:-2] ]

    for PD in pds:
        for project_name in list(Day_Renewal_Binary_Dict.keys()):
            matches = re.findall(r"[A-Z_\d]+\'\)", project_name)
            Hydro_Capital_Cost_per_aba[PD+'.'+short_name_to_ABA[matches[0][:-2]]] +=( Day_Renewal_Binary_Dict[project_name])* cost_renewal [matches[0][:-2] ]

    for PD in pds:
        for project_name in list(Month_Renewal_Binary_Dict.keys()):
            matches = re.findall(r"[A-Z_\d]+\'\)", project_name)
            Hydro_Capital_Cost_per_aba[PD+'.'+short_name_to_ABA[matches[0][:-2]]] +=( Month_Renewal_Binary_Dict[project_name])* cost_renewal [matches[0][:-2] ]

    h_new = list(set(Supply.iloc[:][1]))
    gendata = pd.read_csv (r'generation_type_data.csv',header=0 )
    allplants=list(gendata.iloc[:]['Type'])

    fuel_co2=list(gendata.iloc[:]['fuel_co2'])
    fuel_co2_obps_2025=list(gendata.iloc[:]['fuel_co2_obps_2025'])
    fuel_co2_obps_2030=list(gendata.iloc[:]['fuel_co2_obps_2030'])

    tplant_fuel_co2_dict_cer = {tplant:fuel_co2[allplants.index(tplant)] for tplant in allplants}
    tplant_fuel_co2_dict_obps_2025 = {tplant:fuel_co2_obps_2025[allplants.index(tplant)] for tplant in allplants}
    tplant_fuel_co2_dict_obps_2030 = {tplant:fuel_co2_obps_2030[allplants.index(tplant)] for tplant in allplants}
    
    carbon_cost_ABA=np.zeros((len(pds),len(aba),len(tplants)))
    for PD in pds:
        for H in h_new:
            for ABA in aba:
                for TP in tplants:
                    ##carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=supply.iloc[pds.index(PD)*len(h)*len(aba)*len(tplants)+h.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_cer[TP] * ctax[PD]
                    if PD == '2025': ## In 2025 the OBPS is in effect no matter what
                        carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=Supply.iloc[pds.index(PD)*len(h_new)*len(aba)*len(tplants)+h_new.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_obps_2025[TP] * ctax[PD] * cap_cost_alter 
                    elif PD == '2030': ## In 2030 the OBPS is in effect no matter what
                        carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=Supply.iloc[pds.index(PD)*len(h_new)*len(aba)*len(tplants)+h_new.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_obps_2030[TP] * ctax[PD] * cap_cost_alter 
                    elif flag_cer: ## If CER is on, then full carbon tax is applied 2035-2050
                        carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=Supply.iloc[pds.index(PD)*len(h_new)*len(aba)*len(tplants)+h_new.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_cer[TP] * ctax[PD] * cap_cost_alter 
                    else: # If CER is false, then OBPS carbon tax is applied 2035-2050 with the OBPS 2030 rate
                        carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=Supply.iloc[pds.index(PD)*len(h_new)*len(aba)*len(tplants)+h_new.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_obps_2030[TP] * ctax[PD] * cap_cost_alter 
    
    carbon_ABA=np.zeros((len(pds),len(aba),len(tplants)))
    for PD in pds:
        for H in h_new:
            for ABA in aba:
                for TP in tplants:
                    carbon_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=Supply.iloc[pds.index(PD)*len(h_new)*len(aba)*len(tplants)+h_new.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_cer[TP] 

    carbon_cost_ap=np.zeros((len(pds),len(ap),len(tplants)))
    for AP in ap:
        for PD in pds:
            for ABA in aba:
                for TP in tplants:
                    if AP in ABA:
                        carbon_cost_ap[pds.index(PD),ap.index(AP),tplants.index(TP)]+=carbon_cost_ABA[pds.index(PD),aba.index(ABA),tplants.index(TP)]

    
    variable_om_cost_ABA = {PD+'.'+ABA: {TP:0 for TP in tplants} for PD in pds for ABA in aba}

    for PD in pds:
        for H in h_new:
            for ABA in aba:
                for TP in tplants:
                    variable_om_cost_ABA[PD+'.'+ABA][TP] += Supply.iloc[pds.index(PD)*len(h_new)*len(aba)*len(tplants)+h_new.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * variable_o_m[TP] * cap_cost_alter
    
    fixed_om_cost_ABA = {PD+'.'+ABA: 0 for PD in pds for ABA in aba}
    for PD in pds:
            for ABA in aba:
                for TP in tplants:
                    fixed_om_cost_ABA[PD+'.'+ABA] += Capacity_TP_dict[PD+'.'+TP.lower()+'.'+ABA.lower()]* fixed_o_m[TP]#Capacity_TP[ABA+'.'+PD][allplants.index(TP)] * fixed_o_m[TP]

    wind_ons_capacity_cost = cost_capacity(windonscost, Capacity_Wind_ons)
    wind_ofs_capacity_cost =  cost_capacity(windonscost, Capacity_Wind_ofs)
    solar_capacity_cost =  cost_capacity(solarcost, Capacity_Solar)
    wind_ons_recon_capacity_cost = cost_capacity(windonscost_recon, Capacity_Wind_ons_Recon, pd_only = True)
    wind_ofs_recon_capacity_cost = cost_capacity(windofscost_recon, Capacity_Wind_ofs_Recon, pd_only = True)
    solar_recon_capacity_cost = cost_capacity(solarcost_recon, Capacity_Solar_Recon, pd_only = True)
    FixedOM_wind_ons_capacity_cost = FixedOM_cost_capacity(Capacity_Wind_ons, Capacity_Wind_ons_Recon)
    FixedOM_Solar_capacity_cost = FixedOM_cost_capacity(Capacity_Solar, Capacity_Solar_Recon)
    
    Cost_Dict = {}
    for pd_index in range(len(pds)):
        Cost_Dict[pds[pd_index]] = cost_calculation(pds[pd_index], pd_index)

    for PD_index in range(len(pds)):
        Cost_Dict[pds[PD_index]]["Wind_ons"] = wind_ons_capacity_cost[PD_index]
        Cost_Dict[pds[PD_index]]["Wind_ofs"] =  wind_ofs_capacity_cost[PD_index]
        Cost_Dict[pds[PD_index]]["Solar"] = solar_capacity_cost[PD_index]
        Cost_Dict[pds[PD_index]]["Wind_ons_Recon"] = wind_ons_recon_capacity_cost[PD_index]
        Cost_Dict[pds[PD_index]]["Wind_ofs_Recon"] = wind_ofs_recon_capacity_cost[PD_index]
        Cost_Dict[pds[PD_index]]["Solar_Recon"] = solar_recon_capacity_cost[PD_index]        
        Cost_Dict[pds[PD_index]]["FixedOM Wind_ons"] = FixedOM_wind_ons_capacity_cost[PD_index]
        Cost_Dict[pds[PD_index]]["FixedOM Solar"] = FixedOM_Solar_capacity_cost[PD_index]
        for ABA in aba:
            
            for TP in tplants:
                Cost_Dict[pds[PD_index]]["Variable_OM."+TP][ABA] = variable_om_cost_ABA[pds[PD_index]+'.'+ABA][TP]
            
            #Cost_Dict[pds[PD_index]]["Variable OM"][ABA] += sum(list(variable_om_cost_ABA[pds[PD_index]+'.'+ABA].values()))

            Cost_Dict[pds[PD_index]]["Carbon Cost"][ABA] += sum(carbon_cost_ABA[PD_index,aba.index(ABA)])

            #Cost_Dict[pds[PD_index]]["Carbon"][ABA] += sum(carbon_ABA[PD_index,aba.index(ABA)])
            Cost_Dict[pds[PD_index]]["Fixed OM"][ABA] += fixed_om_cost_ABA[pds[PD_index]+'.'+ABA]
            Cost_Dict[pds[PD_index]]["Hydro Capital Cost"][ABA] += Hydro_Capital_Cost_per_aba[pds[PD_index]+'.'+ABA]

    #calculate the annuity for capital costs of each thermal plant
    capital_cost_types = ['Wind_ons','Wind_ofs','Solar','Wind_ons_Recon','Wind_ofs_Recon','Solar_Recon']
    capital_cost_types.extend(tplants)

    for PD in pds:
        for KEY in capital_cost_types:
            for ABA in aba:
                if PD != pds[0]:
                    Cost_Dict[PD][KEY][ABA] = Cost_Dict[PD][KEY][ABA] + Cost_Dict[pds[pds.index(PD)-1]][KEY][ABA]

    
    VRE_types = ['Wind_ons','Solar']
    for PD in pds:
        for KEY in VRE_types:
            for ABA in aba:
                if PD != pds[0]:
                    Cost_Dict[PD]["FixedOM "+KEY][ABA] = Cost_Dict[PD]["FixedOM "+KEY][ABA] + Cost_Dict[pds[pds.index(PD)-1]]["FixedOM "+KEY][ABA]
    '''
    for PD in pds:
        for KEY in tplants:
            for ABA in aba:
                if PD != pds[0]:
                    Cost_Dict[PD]["Fixed_OM."+KEY][ABA] = Cost_Dict[PD]["Fixed_OM."+KEY][ABA] + Cost_Dict[pds[pds.index(PD)-1]]["Fixed_OM."+KEY][ABA]

    for PD in pds:
            for ABA in aba:
                if PD != pds[0]:
                    Cost_Dict[PD]["Fixed_OM_Thermal"][ABA] = Cost_Dict[PD]["Fixed_OM_Thermal"][ABA] + Cost_Dict[pds[pds.index(PD)-1]]["Fixed_OM_Thermal"][ABA]
    '''

    for PD in pds:
        for ABA in aba:
            Sum = 0
            for Key in [key for key in Cost_Dict[PD]][:67]:
                if not isinstance(Cost_Dict[PD][Key],str) and not isinstance(Cost_Dict[PD][Key],int):
                    Sum += Cost_Dict[PD][Key][ABA]
            Cost_Dict[PD]["Total"][ABA] = Sum
            if PD != pds[-1]:
                Cost_Dict[PD]["Present"][ABA] = future_to_present(annuity_to_present(Sum, discount, int(pds[-1])-int(PD)), discount, (int(PD)-1)-2021)
            elif PD == pds[-1]:
                Cost_Dict[PD]["Present"][ABA] = future_to_present(Sum, discount_2050, int(PD)-2021)
    
    prov_data = {'__':list(Cost_Dict[pds[0]].keys())}

    for PD in pds:
        for ABA in aba:
            prov_data.update({ABA+'.'+PD:[]})

    for PD in pds:
        for measure in Cost_Dict[PD]:
            if isinstance(Cost_Dict[PD][measure],dict):
                print(Cost_Dict[PD][measure])
                for ABA in aba:
                    prov_data[ABA+'.'+PD].append(Cost_Dict[PD][measure][ABA]) 
            else:
                for ABA in aba:
                    prov_data[ABA+'.'+PD].append(" ") 

    prov_data_df = pd.DataFrame(prov_data, copy=False)
    prov_data_df.to_excel(writer, sheet_name="Cost")
    writer.save()

if __name__ == "__main__":
    main()