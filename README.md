# COPPER Cost

### What does Cost.py do?
**Cost.py uses COPPER files (output and input) as input, both COPPER output and input should be placed in the same folder as Cost.py.**
#### COPPER Input Files:
2023-0504-COPPER-Updates-List.xlsx

230630-GN-DataInventory - Custom - 07 JUL 2023.xlsx

COPPER_configuration.xlsx

all_solar_locations.csv

annual_growth.csv

annual_growth_172.csv

annual_growth_1_5.csv

annual_growth_225.csv

annual_growth_high.csv

annual_growth_low.csv

ba_set.csv

biomass_max_aba_limit.csv

capacity_value.csv

coordinate.csv

days.csv

demand-dominic.csv

demand-raw.csv

demand.csv

distance_to_grid.csv

extant_capacity - Copy.csv

extant_capacity - Good.csv

extant_capacity.csv

extant_capacity_BAU.csv

extant_capacity_CER.csv

extant_capacity_contracted.csv

extant_solar - Copy.csv

extant_solar.csv

extant_storage - Copy.csv

extant_storage.csv

extant_transmission - Copy.csv

extant_transmission.csv

extant_wind - Copy.csv

extant_wind.csv

generation_type_data - Copy.csv

generation_type_data - Old.csv

generation_type_data - Update.csv

generation_type_data - Update.xlsx

generation_type_data.csv

grid_locations.csv

hours.csv

hydro_cf.csv

hydro_new - Copy.csv

hydro_new.csv

map_gl_to_ba.csv

map_gl_to_pr.csv

max_nuclear.csv

max_smr.csv

merra_wind_set_all.csv

population.csv

population_locations.csv

reserve_margin.csv

run_days-12.csv

run_days.csv

run_days_26.csv

run_days_test - Copy.csv

run_days_test.csv

set_map_days_to_hours.csv

set_map_months_to_hours.csv

solar_max_ap_limit.csv

solar_min_ap_limit.csv

solarcf.csv

surface_area_solar.csv

surface_area_wind_ofs.csv

surface_area_wind_ons.csv

technology_evolution.csv

trans_expansion_limits.csv

transmission_map_ba.csv

transmission_map_distance.csv

us_demand.csv

wind_max_ap_limit.csv

wind_min_ap_limit.csv

wind_ofs_max_ap_limit.csv

wind_ofs_min_ap_limit.csv

wind_ons_max_ap_limit.csv

wind_ons_min_ap_limit.csv

wind_solar_capacity_value.csv

wind_solar_location_recon.csv

windcf.csv

#### COPPER Output Files: 

ABA_investment_costs.csv

capacity_solar.csv

capacity_solar_recon.csv

capacity_storage.csv

capacity_thermal.csv

capacity_transmission.csv

capacity_wind_ofs.csv

capacity_wind_ofs_recon.csv

capacity_wind_ons.csv

capacity_wind_ons_recon.csv

COPPER config.txt

day_renewal_binary.csv

dayrenewalout.csv

daystoragehydroout.csv

month_renewal_binary.csv

monthrenewalout.csv

monthstoragehydroout.csv

new_ABA_generation_mix.csv

obj_value.csv

Qualifying_capacity_summer.csv

Qualifying_capacity_winter.csv

Results_summary.xlsx

retire_thermal.csv

ror_renewal_binary.csv

solarout.csv

solver_report.txt

storageenergy.csv

storagein.csv

storageout.csv

supply.csv

Total_installed_ABA.csv

transmission.csv

windofsout.csv

windonsout.csv

### What does Cost.xlsx contain?
 #### Cost.py produces one output file Cost.xlsx, for each period covered it contains an entry for:
 
  Fixed OM Cost for each technology type.
 
  Variable OM Cost for each technology type.
 
  Capital Cost for each technology.
 
  Total Cost of all technologies.
 
  Present Cost of all technologies.

## Getting started
### Prerequisites: 

Python version 3.9.16

Virtual environment framework like venv or Conda 

### Running steps
1) Clone the repo.

2) Create and activate the virtual environment.

2) Run the following: 

        cd "CER-ITC High Sept 22nd"

        pip install -r requirements.txt

        python Cost.py
